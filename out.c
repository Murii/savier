
// Preamble
#define _CRT_SECURE_NO_WARNINGS

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include <assert.h>

typedef unsigned char uchar;
typedef signed char schar;
typedef unsigned short ushort;
typedef unsigned int uint;
typedef unsigned long ulong;
typedef long long llong;
typedef unsigned long long ullong;

typedef uintptr_t uintptr;
typedef size_t usize;
typedef ptrdiff_t ssize;
typedef ullong typeid;

#ifdef _MSC_VER
#define alignof(x) __alignof(x)
#else
#define alignof(x) __alignof__(x)
#endif


// Foreign header files
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <time.h>
#include <limits.h>
#include <stdint.h>
#include <math.h>
#include <stdlib.h>
// Manual preambles
// Forward declarations
typedef struct TypeInfo TypeInfo;
typedef struct TypeFieldInfo TypeFieldInfo;
typedef struct Any Any;
typedef struct src__save_data_parent src__save_data_parent;
typedef struct src__save_data src__save_data;

// Sorted declarations
#line 2 "/home/vmuresan/Programming/savier/system_packages/builtin/misc.rii"
#define RII__VERSION ((int)(28111803))

#line 1 "/home/vmuresan/Programming/savier/system_packages/builtin/typeinfo.rii"
typedef int TypeKind;

#line 2
#define TYPE_NONE ((int)(0))

#line 3
#define TYPE_VOID ((int)((TYPE_NONE) + (1)))

#line 4
#define TYPE_BOOL ((int)((TYPE_VOID) + (1)))

#line 5
#define TYPE_CHAR ((int)((TYPE_BOOL) + (1)))

#line 6
#define TYPE_UCHAR ((int)((TYPE_CHAR) + (1)))

#line 7
#define TYPE_SCHAR ((int)((TYPE_UCHAR) + (1)))

#line 8
#define TYPE_SHORT ((int)((TYPE_SCHAR) + (1)))

#line 9
#define TYPE_USHORT ((int)((TYPE_SHORT) + (1)))

#line 10
#define TYPE_INT ((int)((TYPE_USHORT) + (1)))

#line 11
#define TYPE_UINT ((int)((TYPE_INT) + (1)))

#line 12
#define TYPE_LONG ((int)((TYPE_UINT) + (1)))

#line 13
#define TYPE_ULONG ((int)((TYPE_LONG) + (1)))

#line 14
#define TYPE_LLONG ((int)((TYPE_ULONG) + (1)))

#line 15
#define TYPE_ULLONG ((int)((TYPE_LLONG) + (1)))

#line 16
#define TYPE_FLOAT ((int)((TYPE_ULLONG) + (1)))

#line 17
#define TYPE_DOUBLE ((int)((TYPE_FLOAT) + (1)))

#line 18
#define TYPE_CONST ((int)((TYPE_DOUBLE) + (1)))

#line 19
#define TYPE_PTR ((int)((TYPE_CONST) + (1)))

#line 20
#define TYPE_ARRAY ((int)((TYPE_PTR) + (1)))

#line 21
#define TYPE_STRUCT ((int)((TYPE_ARRAY) + (1)))

#line 22
#define TYPE_UNION ((int)((TYPE_STRUCT) + (1)))

#line 23
#define TYPE_FUNC ((int)((TYPE_UNION) + (1)))

#line 32
struct TypeInfo {
    TypeKind kind;
    int size;
    int align;
    char const ((*name));
    int count;
    typeid base;
    TypeFieldInfo (*fields);
    int num_fields;
};

#line 49
TypeKind typeid_kind(typeid type);

#line 53
int typeid_index(typeid type);

#line 57
usize typeid_size(typeid type);

#line 61
TypeInfo const ((*get_typeinfo(typeid type)));

#line 32 "/home/vmuresan/Programming/savier/src/savier.rii"
#define src__SAVE_DATA_SUB_PARENTS_GROW_BY (8)

#line 33
#define src__SAVE_DATA_GROW_BY (64)

#define src__SAVE_DATA_MAX_ENTRIES (128)

#line 36
#define src__SAVE_DATA_MAX_KEY_LEN (64)

#line 37
#define src__SAVE_DATA_MAX_VALUE_LEN (64)

#line 76
void src__save_data_init(src__save_data (*save), char const ((*path)));

#line 128
void src__save_data_free(src__save_data (*save));

#line 157
char (*src__save_data_serialize_parent(char (*parent_name), char (*key), char (*value), char (*sub_parent), usize len));

#line 165
void src__save_data_write(src__save_data (*save), char const ((*data_to_be_saved)), bool force_override);

#line 264
void src__save_data_read(src__save_data (*save));

#line 96 "/home/vmuresan/Programming/savier/src/main.rii"
int main(int argc, char (*(*argv)));

#line 66 "/home/vmuresan/Programming/savier/src/savier.rii"
void src__priv__savier__init_parent(src__save_data_parent (*p));

#line 95
void src__priv__savier__grow_save_data(src__save_data (*save));

#line 111
void src__priv__savier__grow_sub_parent(src__save_data_parent (*p));

#line 117
void src__priv__savier__free_sub_parents(src__save_data_parent (*p));

#line 176
void src__priv__savier__skip_comments(src__save_data (*save));

#line 186
void src__priv__savier__parse_child(src__save_data (*save), src__save_data_parent (*parent));

#line 23 "/home/vmuresan/Programming/savier/src/main.rii"
extern char (*src__priv__main__data);

#line 62
void src__priv__main__print_sub_parent(src__save_data_parent (*parent));

#line 73
void src__priv__main__print_paires(src__save_data_parent (*parent));

#line 80
void src__priv__main__print_parent(src__save_data_parent (*parent));

#line 88
void src__priv__main__print_all(src__save_data (*save));

#line 26 "/home/vmuresan/Programming/savier/system_packages/builtin/typeinfo.rii"
struct TypeFieldInfo {
    char const ((*name));
    typeid type;
    int offset;
};

#line 70
struct Any {
    void (*ptr);
    typeid type;
};

#line 39 "/home/vmuresan/Programming/savier/src/savier.rii"
struct src__save_data_parent {
    char (name[src__SAVE_DATA_MAX_KEY_LEN]);
    usize id;
    char ((keys[src__SAVE_DATA_MAX_KEY_LEN])[src__SAVE_DATA_MAX_ENTRIES]);
    char ((values[src__SAVE_DATA_MAX_VALUE_LEN])[src__SAVE_DATA_MAX_ENTRIES]);
    usize total;
    #line 46
    bool has_sub_parent;
    src__save_data_parent (*(*sub_parent));
    usize sub_parents;
    usize sub_parent_id;
};

#line 53
struct src__save_data {
    FILE (*raw_file);
    char const ((*path));
    char (*file);
    long file_size;
    int c;
    #line 60
    src__save_data_parent (*(*parents));
    usize count;
    usize len;
};

// Typeinfo
#define TYPEID0(index, kind) ((ullong)(index) | ((kind) << 24ull))
#define TYPEID(index, kind, ...) ((ullong)(index) | (sizeof(__VA_ARGS__) << 32ull) | ((kind) << 24ull))

const TypeInfo *typeinfo_table[64] = {
    [0] = NULL, // No associated type
    [1] = &(TypeInfo){TYPE_VOID, .name = "void", .size = 0, .align = 0},
    [2] = &(TypeInfo){TYPE_BOOL, .size = sizeof(bool), .align = sizeof(bool), .name = "bool"},
    [3] = &(TypeInfo){TYPE_CHAR, .size = sizeof(char), .align = sizeof(char), .name = "char"},
    [4] = &(TypeInfo){TYPE_UCHAR, .size = sizeof(uchar), .align = sizeof(uchar), .name = "uchar"},
    [5] = &(TypeInfo){TYPE_SCHAR, .size = sizeof(schar), .align = sizeof(schar), .name = "schar"},
    [6] = &(TypeInfo){TYPE_SHORT, .size = sizeof(short), .align = sizeof(short), .name = "short"},
    [7] = &(TypeInfo){TYPE_USHORT, .size = sizeof(ushort), .align = sizeof(ushort), .name = "ushort"},
    [8] = &(TypeInfo){TYPE_INT, .size = sizeof(int), .align = sizeof(int), .name = "int"},
    [9] = &(TypeInfo){TYPE_UINT, .size = sizeof(uint), .align = sizeof(uint), .name = "uint"},
    [10] = &(TypeInfo){TYPE_LONG, .size = sizeof(long), .align = sizeof(long), .name = "long"},
    [11] = &(TypeInfo){TYPE_ULONG, .size = sizeof(ulong), .align = sizeof(ulong), .name = "ulong"},
    [12] = &(TypeInfo){TYPE_LLONG, .size = sizeof(llong), .align = sizeof(llong), .name = "llong"},
    [13] = &(TypeInfo){TYPE_ULLONG, .size = sizeof(ullong), .align = sizeof(ullong), .name = "ullong"},
    [14] = &(TypeInfo){TYPE_FLOAT, .size = sizeof(float), .align = sizeof(float), .name = "float"},
    [15] = &(TypeInfo){TYPE_DOUBLE, .size = sizeof(double), .align = sizeof(double), .name = "double"},
    [16] = &(TypeInfo){TYPE_PTR, .size = sizeof(void *), .align = alignof(void *), .base = TYPEID0(1, TYPE_VOID)},
    [17] = NULL, // Enum
    [18] = &(TypeInfo){TYPE_STRUCT, .size = sizeof(TypeFieldInfo), .align = alignof(TypeFieldInfo), .name = "TypeFieldInfo", .num_fields = 3, .fields = (TypeFieldInfo[]) {
        {"name", .type = TYPEID(21, TYPE_PTR, char const (*)), .offset = offsetof(TypeFieldInfo, name)},
        {"type", .type = TYPEID(8, TYPE_INT, int), .offset = offsetof(TypeFieldInfo, type)},
        {"offset", .type = TYPEID(8, TYPE_INT, int), .offset = offsetof(TypeFieldInfo, offset)},
    }},
    [19] = &(TypeInfo){TYPE_STRUCT, .size = sizeof(TypeInfo), .align = alignof(TypeInfo), .name = "TypeInfo", .num_fields = 8, .fields = (TypeFieldInfo[]) {
        {"kind", .type = TYPEID(17, TYPE_NONE, TypeKind), .offset = offsetof(TypeInfo, kind)},
        {"size", .type = TYPEID(8, TYPE_INT, int), .offset = offsetof(TypeInfo, size)},
        {"align", .type = TYPEID(8, TYPE_INT, int), .offset = offsetof(TypeInfo, align)},
        {"name", .type = TYPEID(21, TYPE_PTR, char const (*)), .offset = offsetof(TypeInfo, name)},
        {"count", .type = TYPEID(8, TYPE_INT, int), .offset = offsetof(TypeInfo, count)},
        {"base", .type = TYPEID(8, TYPE_INT, int), .offset = offsetof(TypeInfo, base)},
        {"fields", .type = TYPEID(22, TYPE_PTR, TypeFieldInfo *), .offset = offsetof(TypeInfo, fields)},
        {"num_fields", .type = TYPEID(8, TYPE_INT, int), .offset = offsetof(TypeInfo, num_fields)},
    }},
    [20] = &(TypeInfo){TYPE_CONST, .size = sizeof(char const ), .align = alignof(char const ), .base = TYPEID(3, TYPE_CHAR, char)},
    [21] = &(TypeInfo){TYPE_PTR, .size = sizeof(void *), .align = alignof(void *), .base = TYPEID(20, TYPE_CONST, char const )},
    [22] = &(TypeInfo){TYPE_PTR, .size = sizeof(void *), .align = alignof(void *), .base = TYPEID(18, TYPE_STRUCT, TypeFieldInfo)},
    [23] = &(TypeInfo){TYPE_CONST, .size = sizeof(TypeInfo const ), .align = alignof(TypeInfo const ), .base = TYPEID(19, TYPE_STRUCT, TypeInfo)},
    [24] = &(TypeInfo){TYPE_PTR, .size = sizeof(void *), .align = alignof(void *), .base = TYPEID(23, TYPE_CONST, TypeInfo const )},
    [25] = &(TypeInfo){TYPE_PTR, .size = sizeof(void *), .align = alignof(void *), .base = TYPEID(24, TYPE_PTR, TypeInfo const (*))},
    [26] = NULL, // Function
    [27] = NULL, // Function
    [28] = NULL, // Function
    [29] = NULL, // Function
    [30] = &(TypeInfo){TYPE_STRUCT, .size = sizeof(Any), .align = alignof(Any), .name = "Any", .num_fields = 2, .fields = (TypeFieldInfo[]) {
        {"ptr", .type = TYPEID(16, TYPE_PTR, void *), .offset = offsetof(Any, ptr)},
        {"type", .type = TYPEID(8, TYPE_INT, int), .offset = offsetof(Any, type)},
    }},
    [31] = &(TypeInfo){TYPE_STRUCT, .size = sizeof(src__save_data_parent), .align = alignof(src__save_data_parent), .name = "src__save_data_parent", .num_fields = 9, .fields = (TypeFieldInfo[]) {
        {"name", .type = TYPEID(44, TYPE_ARRAY, char [64]), .offset = offsetof(src__save_data_parent, name)},
        {"id", .type = TYPEID(13, TYPE_ULLONG, ullong), .offset = offsetof(src__save_data_parent, id)},
        {"keys", .type = TYPEID(46, TYPE_ARRAY, char [64][128]), .offset = offsetof(src__save_data_parent, keys)},
        {"values", .type = TYPEID(46, TYPE_ARRAY, char [64][128]), .offset = offsetof(src__save_data_parent, values)},
        {"total", .type = TYPEID(13, TYPE_ULLONG, ullong), .offset = offsetof(src__save_data_parent, total)},
        {"has_sub_parent", .type = TYPEID(2, TYPE_BOOL, bool), .offset = offsetof(src__save_data_parent, has_sub_parent)},
        {"sub_parent", .type = TYPEID(47, TYPE_PTR, src__save_data_parent (**)), .offset = offsetof(src__save_data_parent, sub_parent)},
        {"sub_parents", .type = TYPEID(13, TYPE_ULLONG, ullong), .offset = offsetof(src__save_data_parent, sub_parents)},
        {"sub_parent_id", .type = TYPEID(13, TYPE_ULLONG, ullong), .offset = offsetof(src__save_data_parent, sub_parent_id)},
    }},
    [32] = &(TypeInfo){TYPE_STRUCT, .size = sizeof(src__save_data), .align = alignof(src__save_data), .name = "src__save_data", .num_fields = 8, .fields = (TypeFieldInfo[]) {
        {"raw_file", .type = TYPEID(49, TYPE_PTR, FILE *), .offset = offsetof(src__save_data, raw_file)},
        {"path", .type = TYPEID(21, TYPE_PTR, char const (*)), .offset = offsetof(src__save_data, path)},
        {"file", .type = TYPEID(36, TYPE_PTR, char *), .offset = offsetof(src__save_data, file)},
        {"file_size", .type = TYPEID(10, TYPE_LONG, long), .offset = offsetof(src__save_data, file_size)},
        {"c", .type = TYPEID(8, TYPE_INT, int), .offset = offsetof(src__save_data, c)},
        {"parents", .type = TYPEID(47, TYPE_PTR, src__save_data_parent (**)), .offset = offsetof(src__save_data, parents)},
        {"count", .type = TYPEID(13, TYPE_ULLONG, ullong), .offset = offsetof(src__save_data, count)},
        {"len", .type = TYPEID(13, TYPE_ULLONG, ullong), .offset = offsetof(src__save_data, len)},
    }},
    [33] = &(TypeInfo){TYPE_PTR, .size = sizeof(void *), .align = alignof(void *), .base = TYPEID(32, TYPE_STRUCT, src__save_data)},
    [34] = NULL, // Function
    [35] = NULL, // Function
    [36] = &(TypeInfo){TYPE_PTR, .size = sizeof(void *), .align = alignof(void *), .base = TYPEID(3, TYPE_CHAR, char)},
    [37] = NULL, // Function
    [38] = NULL, // Function
    [39] = &(TypeInfo){TYPE_PTR, .size = sizeof(void *), .align = alignof(void *), .base = TYPEID(36, TYPE_PTR, char *)},
    [40] = NULL, // Function
    [41] = &(TypeInfo){TYPE_PTR, .size = sizeof(void *), .align = alignof(void *), .base = TYPEID(31, TYPE_STRUCT, src__save_data_parent)},
    [42] = NULL, // Function
    [43] = NULL, // Function
    [44] = &(TypeInfo){TYPE_ARRAY, .size = sizeof(char [64]), .align = alignof(char [64]), .base = TYPEID(3, TYPE_CHAR, char), .count = 64},
    [45] = &(TypeInfo){TYPE_ARRAY, .size = sizeof(char [128]), .align = alignof(char [128]), .base = TYPEID(3, TYPE_CHAR, char), .count = 128},
    [46] = &(TypeInfo){TYPE_ARRAY, .size = sizeof(char [64][128]), .align = alignof(char [64][128]), .base = TYPEID(45, TYPE_ARRAY, char [128]), .count = 64},
    [47] = &(TypeInfo){TYPE_PTR, .size = sizeof(void *), .align = alignof(void *), .base = TYPEID(41, TYPE_PTR, src__save_data_parent *)},
    [48] = NULL, // Incomplete: FILE
    [49] = &(TypeInfo){TYPE_PTR, .size = sizeof(void *), .align = alignof(void *), .base = TYPEID0(48, TYPE_NONE)},
    [50] = NULL, // Function
    [51] = NULL, // Function
    [52] = NULL, // Function
    [53] = NULL, // Function
    [54] = NULL, // Function
    [55] = NULL, // Function
    [56] = NULL, // Function
    [57] = NULL, // Function
    [58] = NULL, // Function
    [59] = NULL, // Function
    [60] = NULL, // Function
    [61] = NULL, // Function
    [62] = NULL, // Function
    [63] = &(TypeInfo){TYPE_PTR, .size = sizeof(void *), .align = alignof(void *), .base = TYPEID(45, TYPE_ARRAY, char [128])},
};

int num_typeinfos = 64;
const TypeInfo **typeinfos = (const TypeInfo **)typeinfo_table;

// Definitions
#line 49 "/home/vmuresan/Programming/savier/system_packages/builtin/typeinfo.rii"
TypeKind typeid_kind(typeid type) {
    return (TypeKind)(((((type) >> (24))) & (0xff)));
}

int typeid_index(typeid type) {
    return (int)(((type) & (0xffffff)));
}

usize typeid_size(typeid type) {
    return (ullong)(((type) >> (32)));
}

TypeInfo const ((*get_typeinfo(typeid type))) {
    int index = typeid_index(type);
    if ((typeinfos) && ((index) < (num_typeinfos))) {
        return typeinfos[index];
    } else {
        return NULL;
    }
}

#line 76 "/home/vmuresan/Programming/savier/src/savier.rii"
void src__save_data_init(src__save_data (*save), char const ((*path))) {
    (save->path) = path;
    (save->file_size) = 0;
    (save->c) = 0;
    (save->len) = 0;
    (save->count) = 1;
    #line 83
    (save->parents) = malloc((sizeof(src__save_data_parent)) * (save->count));
    for (int i = 0; (i) < (save->count); (i)++) {
        src__save_data_parent (*parent) = {0};
        (save->parents[i]) = malloc(sizeof(src__save_data_parent));
        (parent) = save->parents[i];
        src__priv__savier__init_parent(parent);
    }
}

#line 128
void src__save_data_free(src__save_data (*save)) {
    for (usize i = 0; (i) < (save->count); (i)++) {
        src__save_data_parent (*p1) = save->parents[i];
        if (p1->has_sub_parent) {
            src__priv__savier__free_sub_parents(p1);
        }
        free(p1);
    }
    #line 137
    free(save->parents);
    #line 139
    if (save->file) {
        free(save->file);
    }
}

#line 157
char (*src__save_data_serialize_parent(char (*parent_name), char (*key), char (*value), char (*sub_parent), usize len)) {
    #line 159
    char (*buffer) = malloc(len);
    sprintf(buffer, "%s:{ \n"
    "%s=%s; \n"
    "%s }\n", parent_name, key, value, sub_parent);
    #line 161
    return buffer;
}

#line 165
void src__save_data_write(src__save_data (*save), char const ((*data_to_be_saved)), bool force_override) {
    (save->raw_file) = fopen(save->path, ((force_override) == (true) ? "w" : "a"));
    #line 168
    if (!(save->raw_file)) {
        fprintf(stderr, "Error opening file: %s\n", save->path);
    }
    fprintf(save->raw_file, "%s", data_to_be_saved);
    fclose(save->raw_file);
}

#line 264
void src__save_data_read(src__save_data (*save)) {
    (save->raw_file) = fopen(save->path, "r");
    if (!(save->raw_file)) {
        printf("Error opening file: %s\n", save->path);
    }
    #line 270
    fseek(save->raw_file, 0, SEEK_END);
    long file_len = ftell(save->raw_file);
    fseek(save->raw_file, 0, SEEK_SET);
    #line 274
    (save->file_size) = file_len;
    #line 276
    (save->file) = malloc(file_len);
    fread(save->file, 1, file_len, save->raw_file);
    #line 279
    char (name[src__SAVE_DATA_MAX_KEY_LEN]) = {0};
    int ni = 0;
    #line 282
    if ((save->file[save->c]) != ('[')) {
        fprintf(stderr, "Error: Expected \'[\' at the begginig of file, got %c\n", save->file[save->c]);
        return;
    } else {
        (save->c)++;
    }
    #line 289
    while (((save->c) < (file_len)) && ((save->file[save->c]) != (']'))) {
        src__save_data_parent (*parent) = {0};
        if ((save->len) >= (save->count)) {
            src__priv__savier__grow_save_data(save);
        }
        (parent) = save->parents[save->len];
        #line 296
        while (isspace(save->file[save->c])) {
            (save->c)++;
        }
        #line 300
        if (((save->file[save->c]) != (':')) && ((save->file[save->c]) != ('#'))) {
            (name[(ni)++]) = save->file[save->c];
            if ((ni) > (src__SAVE_DATA_MAX_KEY_LEN)) {
                fprintf(stderr, "Error: Parent\'s name is bigger than what we can store, %s\n", name);
            }
        } else if ((save->file[save->c]) == (':')) {
            (save->c)++;
            strcpy(parent->name, name);
            memset(name, 0, src__SAVE_DATA_MAX_KEY_LEN);
            (ni) = 0;
            if ((save->file[save->c]) != ('{')) {
                fprintf(stderr, "Error: expected \'{\' when parsing parent: %s, got %c\n", parent->name, save->file[save->c]);
                return;
            } else {
                (save->c)++;
                src__priv__savier__parse_child(save, parent);
                (save->len)++;
            }
        } else if ((save->file[save->c]) == ('#')) {
            src__priv__savier__skip_comments(save);
        }
        (save->c)++;
    }
    #line 324
    fclose(save->raw_file);
}

#line 96 "/home/vmuresan/Programming/savier/src/main.rii"
int main(int argc, char (*(*argv))) {
    #line 98
    src__save_data save = {0};
    src__save_data_init(&(save), "player.stat");
    #line 101
    char (*manually) = src__save_data_serialize_parent("programatically1", "foo", "42", "", 32);
    char (*manually2) = src__save_data_serialize_parent("programatically2", "foo", "43", manually, 64);
    #line 104
    src__save_data_write(&(save), src__priv__main__data, true);
    src__save_data_write(&(save), manually, false);
    src__save_data_write(&(save), manually2, false);
    src__save_data_write(&(save), "]", false);
    #line 110
    src__save_data_read(&(save));
    #line 112
    printf("Total parents: %zu\n", save.len);
    #line 114
    src__priv__main__print_all(&(save));
    #line 116
    src__save_data_free(&(save));
    free(manually);
    free(manually2);
    return 0;
}

#line 66 "/home/vmuresan/Programming/savier/src/savier.rii"
void src__priv__savier__init_parent(src__save_data_parent (*p)) {
    (p->sub_parent) = malloc((sizeof(src__save_data_parent)) * (src__SAVE_DATA_SUB_PARENTS_GROW_BY));
    (p->id) = 0;
    (p->total) = 0;
    (p->sub_parents) = 0;
    (p->has_sub_parent) = false;
    (p->sub_parent_id) = 0;
}

#line 95
void src__priv__savier__grow_save_data(src__save_data (*save)) {
    usize new_size = (save->count) + (((sizeof(src__save_data_parent)) * (src__SAVE_DATA_GROW_BY)));
    usize new_count = (save->count) + (src__SAVE_DATA_GROW_BY);
    #line 99
    (save->parents) = realloc(save->parents, new_size);
    for (ullong i = save->count; (i) < (new_count); (i)++) {
        src__save_data_parent (*parent) = {0};
        (save->parents[i]) = malloc(sizeof(src__save_data_parent));
        (parent) = save->parents[i];
        src__priv__savier__init_parent(parent);
    }
    (save->count) = new_count;
}

#line 111
void src__priv__savier__grow_sub_parent(src__save_data_parent (*p)) {
    usize new_size = (p->sub_parents) + (((sizeof(src__save_data_parent)) * (src__SAVE_DATA_SUB_PARENTS_GROW_BY)));
    (p->sub_parent) = realloc(p->sub_parent, new_size);
}

#line 117
void src__priv__savier__free_sub_parents(src__save_data_parent (*p)) {
    for (usize j = 0; (j) < (p->sub_parents); (j)++) {
        src__save_data_parent (*p1) = p->sub_parent[j];
        if (p1->has_sub_parent) {
            src__priv__savier__free_sub_parents(p1);
        }
        free(p1);
    }
}

#line 176
void src__priv__savier__skip_comments(src__save_data (*save)) {
    #line 178
    (save->c)++;
    while ((save->file[save->c]) != ('#')) {
        (save->c)++;
    }
    (save->c)++;
}

#line 186
void src__priv__savier__parse_child(src__save_data (*save), src__save_data_parent (*parent)) {
    int ki = 0;
    int vi = 0;
    char (key[src__SAVE_DATA_MAX_KEY_LEN]) = {0};
    char (value[src__SAVE_DATA_MAX_VALUE_LEN]) = {0};
    #line 192
    while ((save->file[save->c]) != ('}')) {
        #line 194
        if ((((save->file[save->c]) != ('#')) && ((save->file[save->c]) != ('='))) && ((save->file[save->c]) != (':'))) {
            if (!(isspace(save->file[save->c]))) {
                (key[(ki)++]) = save->file[save->c];
            }
            if ((ki) > (src__SAVE_DATA_MAX_KEY_LEN)) {
                fprintf(stderr, "Error: The key\'s size, %s, you\'re trying to read is bigger than what we can store\n", key);
                return;
            }
        } else if ((save->file[save->c]) == (':')) {
            (save->c)++;
            (parent->has_sub_parent) = true;
            (parent->sub_parent[parent->sub_parents]) = malloc(sizeof(src__save_data_parent));
            #line 207
            src__save_data_parent (*p) = parent->sub_parent[parent->sub_parents];
            (parent->sub_parent_id)++;
            if (((parent->sub_parents) % (src__SAVE_DATA_SUB_PARENTS_GROW_BY)) == (0)) {
                src__priv__savier__grow_sub_parent(parent);
            }
            src__priv__savier__init_parent(p);
            strcpy(p->name, key);
            if ((save->file[save->c]) != ('{')) {
                fprintf(stderr, "Error: expected \'{\' when parsing parent: %s, got %c\n", parent->name, save->file[save->c]);
                return;
            }
            (save->c)++;
            src__priv__savier__parse_child(save, p);
            (p->id) = parent->sub_parent_id;
            (parent->sub_parent[parent->sub_parents]) = p;
            (parent->sub_parents)++;
            memset(key, 0, src__SAVE_DATA_MAX_KEY_LEN);
            (ki) = 0;
        } else if ((save->file[save->c]) == ('=')) {
            (save->c)++;
            while ((save->file[save->c]) != (';')) {
                #line 229
                if (!(isspace(save->file[save->c]))) {
                    (value[(vi)++]) = save->file[save->c];
                }
                #line 233
                if ((vi) > (src__SAVE_DATA_MAX_VALUE_LEN)) {
                    fprintf(stderr, "Error: Value\'s size you\'re trying to read is bigger than what we can store for key: %s\n", key);
                    return;
                }
                #line 238
                (save->c)++;
            }
            strcpy(parent->keys[parent->total], key);
            strcpy(parent->values[parent->total], value);
            #line 243
            (parent->total)++;
            if ((parent->total) > (src__SAVE_DATA_MAX_ENTRIES)) {
                fprintf(stderr, "Error: You\'re trying to parse more data than reserved space for this parent: %s!", parent->name);
                return;
            }
            #line 249
            memset(value, 0, src__SAVE_DATA_MAX_VALUE_LEN);
            memset(key, 0, src__SAVE_DATA_MAX_KEY_LEN);
            (ki) = 0;
            (vi) = 0;
            #line 254
            (save->c)++;
        } else if ((save->file[save->c]) == ('#')) {
            src__priv__savier__skip_comments(save);
        }
        (save->c)++;
    }
    (parent->id) = save->len;
}

char (*src__priv__main__data) = 
    "[\n"
    "player:{\n"
    "player_x=200;\n"
    "player_y=300;\n"
    "    #minor:{\n"
    "    a=1;\n"
    "    b=3;\n"
    "    This has been commented out\n"
    "    }#\n"
    "   \n"
    "    #This is a comment#\n"
    "    minor2:{\n"
    "    hah=  vlad;\n"
    "        minor2_sub_minor1:{\n"
    "            heh=dalv;\n"
    "            deep:{\n"
    "                foo=bar;\n"
    "                foo2 = bar2;\n"
    "                deepest_one:{\n"
    "                    foofoo=barbar;\n"
    "                    foo33 =     batman;\n"
    "                }\n"
    "            }\n"
    "        }\n"
    "        minor2_sub_minor2:{\n"
    "            heha=lvda;\n"
    "        }\n"
    "    } \n"
    "}\n"
    "\n"
    "# A comment #\n"
    "enemy:{\n"
    "    damage=10;\n"
    "    speed=120;\n"
    "}\n"
    "\n";
#line 62 "/home/vmuresan/Programming/savier/src/main.rii"
void src__priv__main__print_sub_parent(src__save_data_parent (*parent)) {
    for (int i = 0; (i) < (parent->sub_parents); (i)++) {
        src__save_data_parent (*p) = parent->sub_parent[i];
        src__priv__main__print_paires(p);
        if (p->has_sub_parent) {
            src__priv__main__print_sub_parent(p);
        }
    }
}

#line 73
void src__priv__main__print_paires(src__save_data_parent (*parent)) {
    for (int i = 0; (i) < (parent->total); (i)++) {
        printf("Key: %s, Value: %s in parent: %s, id: %zu \n", parent->keys[i], parent->values[i], parent->name, parent->id);
    }
}

#line 80
void src__priv__main__print_parent(src__save_data_parent (*parent)) {
    src__priv__main__print_paires(parent);
    if (parent->has_sub_parent) {
        src__priv__main__print_sub_parent(parent);
    }
}

#line 88
void src__priv__main__print_all(src__save_data (*save)) {
    for (int i = 0; (i) < (save->len); (i)++) {
        src__save_data_parent (*p) = save->parents[i];
        src__priv__main__print_parent(p);
    }
}

// Foreign source files
